import { combineReducers } from 'redux';
import item from './item';
import { persistReducer } from 'redux-persist';
import localForage from 'localforage';

const itemPersistConfig = {
    key: 'item',
    storage: localForage,
}

const shiba = combineReducers({
    item: persistReducer(itemPersistConfig,item),
});

export default shiba;