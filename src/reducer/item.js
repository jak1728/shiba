const click_count = (state = [], action) => {
    switch (action.type) {
      case 'COUNT':
        return { ...state,
          num: action.num,
        } 
      default:
        return state
    }
  }
  
  export default click_count