import React, { Component } from 'react';
import Header from './headerNoIcon';
import DataNotFound from './notfound';
import Promise from 'promise';
import { NavLink } from 'react-router-dom';

var total = 0;
class Payment extends Component {

  constructor(props) {
    super(props);

    this.state = {
      cart : (JSON.parse(window.localStorage.getItem("cart")) != null) ? JSON.parse(window.localStorage.getItem("cart")).length:0,
      discount : 0,
      total : 0
    }

    this.calc = this.calc.bind(this);
    this.summ = this.summ.bind(this);
    this.del = this.del.bind(this);
    this.amountChange = this.amountChange.bind(this);
    this.discount = this.discount.bind(this);
  }

  componentDidMount(){
    Promise.all([
      this.discount()
    ]).then(() => {
      this.calc();  
    });
  }

  calc(){
    total = 0;
    let purchase = JSON.parse(window.localStorage.getItem('cart'));

    if(purchase != null){
        purchase.forEach(this.summ);
    }


  }

  summ(item, index){
    total += parseFloat(item.price) * parseInt(item.amount);

    this.setState({
      total : total
    });
  }

  del(item){
    let purchase = JSON.parse(window.localStorage.getItem('cart'));

    let purchaseNew = purchase.filter(x => x.id !== item.id);
    
    if(purchaseNew.length === 0){
      let $this = this;

      Promise.all([
        window.localStorage.removeItem('cart')  
      ]).then(() => {
        $this.setState({
          discount : 0,
          cart : 0,
          total : 0,
          change : 0
        });
      });
    }else{
      let $this = this;

      Promise.all([
        window.localStorage.setItem('cart',JSON.stringify(purchaseNew))  
      ]).then(() => {

        Promise.all([
          $this.discount()
        ]).then(() => {
          $this.calc();  
          $this.calChange();
          $this.setState({
            cart : purchaseNew.length
          });
        });
      });
    }
  }

  amountChange(item){
    let purchase = JSON.parse(window.localStorage.getItem('cart'));

    if(purchase.find(x => x.id === item.id)){
      purchase.map(itm => {
          if(itm.id === item.id){
              itm.amount = parseInt(document.getElementById("amount_"+item.id).value);
          }  
          return 0;
      });

      let $this = this;

      Promise.all([
        window.localStorage.setItem('cart',JSON.stringify(purchase))  
      ]).then(() => {
        Promise.all([
          $this.discount()
        ]).then(() => {
          $this.calc();
          $this.calChange();
        });
      });
    }
  }

  discount(){

      let booklist = [
        "9781408855652",
        "9781408855669",
        "9781408855676",
        "9781408855683",
        "9781408855690",
        "9781408855706",
        "9781408855713"
      ];

      let purchase = JSON.parse(window.localStorage.getItem('cart'));

      let purchaseNew = purchase.filter((x) => {
        return booklist.includes(x.id);
      });

      var percent = 0;

      switch(purchaseNew.length){
        case 2 : percent = 10; break;
        case 3 : percent = 11; break;
        case 4 : percent = 12; break;
        case 5 : percent = 13; break;
        case 6 : percent = 14; break;
        case 7 : percent = 15; break;
        default : percent = 0; break;
      }

      let amountNew = [];
      purchaseNew.map((x,i) => {
        return amountNew[i] = x.amount; 
      });;

      amountNew.sort();

      let amountMin = amountNew[0];
      let totalDis = 0;
      purchaseNew.map((x,i) => {
          return totalDis += x.price * amountMin;
      });

      let totalWithPercent = ((totalDis * percent) / 100);

      this.setState({
        discount : totalWithPercent
      });
  }

  calChange(){
      let change = document.getElementById('payment').value - (this.state.total - this.state.discount); 

      this.setState({
        change : change
      });
  }

  clearData(){
    window.localStorage.removeItem('cart');

    this.setState({
      discount : 0,
      cart : 0,
      total : 0,
      change : 0
    });
  }

  render(){

    let purchase = JSON.parse(window.localStorage.getItem('cart'));

    let trlist = null;

    if(purchase !== null){
      trlist = purchase.map((items,index) => {
          return (
          <tr key={items.id}>
              <td>{items.title}</td>            
              <td>{items.price} บาท</td>
              <td><input className="amount" id={"amount_"+items.id} type="number" name={"amount_"+items.id} maxLength="2" min="1" max="99" defaultValue={items.amount} onChange={() => this.amountChange(items)} /></td>
              <td><i className="fa fa-minus-circle del" aria-hidden="true" onClick={() => this.del(items)}></i></td>
          </tr>
          )
      });
    }else{
      trlist = <DataNotFound colspan="5" />
    }

    return (
    <div className="App">
      <Header />
      <div className="item-list-purchase item-list list-table">
        <table className="list">
          <thead>
            <tr>
              <td>Title</td>
              <td>Price</td>
              <td>Amount</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            {trlist}
          </tbody>
        </table>
          <p className="summary">Discount : <span className="discount-result">{this.state.discount}</span></p>
          <p className="summary">Net : <span id="total-result">{this.state.total - this.state.discount}</span></p>
          <p className="summary">Payment : <input type="text" name="payment" id="payment" onBlur={() => this.calChange()} /></p>
          <p className="summary">Change : <span>{this.state.change}</span></p>
          <div style={{clear:"both"}}></div>
        </div>
        <NavLink to="/" className="btn back">Back</NavLink>
        &nbsp;&nbsp;
        <a href="#cleardata" className="btn cleardatat" onClick={() => this.clearData()}>ClearData</a>
    </div>
    )
  }
}

export default Payment;
