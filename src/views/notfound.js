import React from 'react';

const DataNotFound = props => (
    <tr key="notfound">
        <td colSpan={props.colspan}>Data Not Found</td>
    </tr>
)

export default DataNotFound;