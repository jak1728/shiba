import React , { Component } from 'react';
import './App.css';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart : null,
            click_count : 0
        }
    }

    componentDidMount(){
        this.setState({
            cart : (JSON.parse(window.localStorage.getItem("cart")) != null) ? JSON.parse(window.localStorage.getItem("cart")).length:0
        });
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.item.num !== this.state.click_count){
            this.setState({
                cart : (JSON.parse(window.localStorage.getItem("cart")) != null) ? JSON.parse(window.localStorage.getItem("cart")).length:0,
                click_count : prevProps.item.num
            });
        }
    }

    render(){
        return (
            <>
            <div className="header">
                Shiba book shop
            </div>
            <div className="cart">
                <i className="fa fa-shopping-cart cart-add" aria-hidden="true"></i>
                <p className="cart-count">{this.state.cart}</p>
            </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        item : state.item
    };
}

export default withRouter(connect(mapStateToProps)(Header));