import React, { Component } from 'react';
import axios from 'axios';
import DataNotFound from './notfound';
import * as itemActions from '../action/item';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

var i = 0;

class Items extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list : []
        }

        this.addToCart = this.addToCart.bind(this);
    }

    componentDidMount(){

        const $this = this;

        axios.get('https://api.jsonbin.io/b/5f3154b06f8e4e3faf2f99de?fbclid=IwAR2uMBCdw1067r7n9Pmd4Zbr4kttBmtuplfwtZqPuTNN0Bie81p4NbwTAW8')
            .then(function (response) {
                $this.setState({
                    list : response.data.books
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    addToCart(item){

        i++;

        let old = window.localStorage.getItem('cart');

        if(old != null){

            let oldParse = JSON.parse(old);

            let items_add = {
                id : item.id,
                title : item.title,
                price : item.price,
                amount : parseInt(document.getElementById("amount_"+item.id).value)
            };

            if(oldParse.length >= 1){

                if(oldParse.find(x => x.id === item.id)){

                    oldParse.map(itm => {
                        if(itm.id === item.id){
                            itm.amount = parseInt(itm.amount) + parseInt(document.getElementById("amount_"+item.id).value);
                        }  
                        return 0;
                    });

                    window.localStorage.setItem('cart', JSON.stringify(oldParse));
                }else{
                    let combine = [].concat(oldParse,items_add);
                    window.localStorage.setItem('cart', JSON.stringify(combine));
                }
            }
        }else{
            let items_add = [{
                id : item.id,
                title : item.title,
                price : item.price,
                amount : parseInt(document.getElementById("amount_"+item.id).value)
            }]
    
            window.localStorage.setItem('cart', JSON.stringify(items_add));
        }
        
        this.props.dispatch(itemActions.click_count(i));
    }

    render(){

        let trlist = null;

        if(this.state.list.length > 0){
        trlist = this.state.list.map((items,index) => {
            return (
            <tr key={items.id}>
                <td><img src={items.cover} width="150" alt="" /></td>
                <td>{items.title}</td>            
                <td>{items.price} บาท</td>
                <td><input className="amount" id={"amount_"+items.id} type="number" name={"amount_"+items.id} maxLength="2" min="1" max="99" defaultValue="1" /></td>
                <td><i className="fa fa-cart-plus cart-add" aria-hidden="true" onClick={() => this.addToCart(items)}></i></td>
            </tr>
            )
        });
        }else{
            trlist = <DataNotFound colspan="5" />
        }

        return (
            <div className="list-table">
                <table className="list">
                    <thead>
                        <tr>
                            <td>Thumbnail</td>
                            <td>Title</td>
                            <td>Price</td>
                            <td>Amount</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        {trlist}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default withRouter(connect()(Items));