import React from 'react';
import Header from './header';
import Items from './items';
import './App.css';
import { NavLink } from 'react-router-dom';

const App = () => {
  return (
    <div className="App">
      <Header />
      <div className="item-list">
        <Items />
      </div>
      <NavLink to="/payment" className="btn payment">Payment</NavLink>
      <br/>
    </div>
  );
}

export default App;
