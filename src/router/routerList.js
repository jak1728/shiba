import React from 'react';
import itemlist from '../views/App';
import payment from '../views/Payment';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

const RouterList = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={itemlist} />
      <Route exact path="/payment" component={payment} />
      <Route component={NotFound} />
    </Switch>
  </Router>  
)

const Status = ({ code, children }) => (
  <Route render={({ staticContext }) => {
    if (staticContext)
      staticContext.status = code
    return children
  }}/>
)

const NotFound = () => (
  <Status code={404}>
    <div className="theme1">
      <h1>Sorry, can't find that.</h1>
    </div>
  </Status>
)

export default RouterList;